from django.contrib import admin
from app.models import Question, Answer

admin.site.register(Question)
admin.site.register(Answer)
