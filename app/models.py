from django.db import models
from django.contrib.auth.models import User


class Question(models.Model):
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    votes = models.IntegerField(default=0)
    owner = models.ForeignKey(User)

    def __str__(self):
        return self.text

    def vote_up(self):
        self.votes += 1

    def vote_down(self):
        self.votes -= 1


class Answer(models.Model):
    question = models.ForeignKey(Question)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    rating = models.IntegerField(default=0)
    owner = models.ForeignKey(User)

    def rating_up(self):
        self.rating += 1

    def rating_down(self):
        self.rating -= 1

    def __str__(self):
        if len(self.text) > 50:
            return self.text[:50] + "..."
        else:
            return self.text


class Vote(models.Model):
    question = models.ForeignKey(Question)
    vote = models.IntegerField()
    owner = models.ForeignKey(User)


class AVote(models.Model):
    answer = models.ForeignKey(Answer)
    vote = models.IntegerField()
    owner = models.ForeignKey(User)

