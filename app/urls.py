from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^question/(?P<question_id>\d+)/$', views.question, name='question'),
    url(r'^question/(?P<question_id>\d+)/up$', views.question_vote_up, name='question_vote_up'),
    url(r'^question/(?P<question_id>\d+)/down$', views.question_vote_down, name='question_vote_down'),
    url(r'^answer/(?P<answer_id>\d+)/up$', views.answer_vote_up, name='answer_vote_up'),
    url(r'^answer/(?P<answer_id>\d+)/down$', views.answer_vote_down, name='answer_vote_down'),
]
