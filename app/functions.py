from .models import Vote, AVote


def add_vote(request, question_obj, value):
    vote = Vote()
    vote.question = question_obj
    vote.owner = request.user
    vote.vote = value
    vote.save()


def add_answer_vote(request, answer_obj, value):
    avote = AVote()
    avote.answer = answer_obj
    avote.owner = request.user
    avote.vote = value
    avote.save()
