from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from .models import Question, Answer, Vote, AVote
from .forms import QuestionForm, AnswerForm
from .functions import add_vote, add_answer_vote


def index(request):
    if request.method == 'POST' and request.user.is_authenticated():
        form = QuestionForm(request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.owner = request.user
            new_entry.save()
            return HttpResponseRedirect(reverse('app:index'))
    else:
        form = QuestionForm()

    questions = Question.objects.order_by('date_added')
    context = {'questions': questions, 'form': form}
    return render(request, 'app/index.html', context)


def question(request, question_id):
    question_obj = get_object_or_404(Question, pk=question_id)
    if request.method == 'POST' and request.user.is_authenticated():
        form = AnswerForm(request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.owner = request.user
            new_entry.question = question_obj
            new_entry.save()
            return HttpResponseRedirect(reverse('app:question', args=[question_id]))
    else:
        form = AnswerForm()

    answers = question_obj.answer_set.order_by('date_added')
    context = {'question': question_obj, 'answers': answers, 'form':form}
    return render(request, 'app/question.html', context)


@login_required
def question_vote_up(request, question_id):
    question_obj = get_object_or_404(Question, pk=question_id)
    question_obj.vote_up()
    question_obj.save()
    add_vote(request, question_obj, 1)
    return HttpResponseRedirect(reverse('app:index'))


@login_required
def question_vote_down(request, question_id):
    question_obj = get_object_or_404(Question, pk=question_id)
    question_obj.vote_down()
    question_obj.save()
    add_vote(request, question_obj, -1)
    return HttpResponseRedirect(reverse('app:index'))


@login_required
def answer_vote_up(request, answer_id):
    answer_obj = get_object_or_404(Answer, pk=answer_id)
    answer_obj.rating_up()
    answer_obj.save()
    add_answer_vote(request, answer_obj, 1)
    return HttpResponseRedirect(reverse('app:question', args=[answer_obj.question.id]))


@login_required
def answer_vote_down(request, answer_id):
    answer_obj = get_object_or_404(Answer, pk=answer_id)
    answer_obj.rating_down()
    answer_obj.save()
    add_answer_vote(request, answer_obj, -1)
    return HttpResponseRedirect(reverse('app:question', args=[answer_obj.question.id]))
